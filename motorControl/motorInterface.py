from .motorCommander import MotorCommander
import numpy as np


class Movement():
    def __init__(self, position=0, relative=True):
        self.position = position
        self.relative = relative


class Status():
    def __init__(self):
        self.actualPosition = 0
        self.targetPosition = 0
        self.remainingMovementInitial = 0
        self.remainingMovement = 0
        self.remainingMovementRelative = 0

    def calcRemainingMovement(self, initial = False):
        if initial:
            self.remainingMovementInitial =  self.targetPosition - self.actualPosition
            self.remainingMovementRelative = 1
            
        if self.remainingMovementInitial != 0:
            self.remainingMovement = self.targetPosition - self.actualPosition
            self.remainingMovementRelative = self.remainingMovement / self.remainingMovementInitial


class Settings():
    def __init__(
        self,
        motor_idx=0,
        module_address=1,
        module_current=3,
        run_current=1,
        standby_current=0.5,
        microsteps=8,
        spindle_slope=5,  # mm/revolution
        speed=50,  # mm/s
        motor_steps=200,  # steps/revolution
        acceleration_factor=3,
        velocity_start=1,
        velocity_stop=2,
        ref_search_speed_factor=0.1,
        ref_switch_speed_factor=0.05,
        right_limit_switch_disable=False,
        left_limit_switch_disable=False,
        swap_limit_switches=False,
        right_limit_switch_polarity=0,
        left_limit_switch_polarity=0,
        ref_search_mode=True
    ):

        self.axis_resolution = motor_steps * microsteps / spindle_slope

        self.motor_idx = motor_idx
        self.module_address = module_address
        self.module_current = module_current

        if run_current > self.module_current or run_current < 0:
            raise ValueError("Run current is not within allowed range.")
        else:
            self.run_current = run_current

        if standby_current > self.module_current or standby_current < 0:
            raise ValueError("Standby current is not within allowed range.")
        else:
            self.standby_current = standby_current

        possible_microsteps = [1, 2, 4, 8, 16, 32, 64, 128, 256]
        if microsteps not in possible_microsteps:
            raise ValueError("Microstep count incorrect.")
        else:
            self.microsteps = microsteps

        self.acceleration_factor = acceleration_factor
        self.ref_search_speed_factor = ref_search_speed_factor
        self.ref_switch_speed_factor = ref_switch_speed_factor

        # TODO: tie to maximum speed?
        self.velocity_start = velocity_start
        self.velocity_stop = velocity_stop

        self.updateMaxSpeed(speed)

        self.right_limit_switch_disable = right_limit_switch_disable
        self.left_limit_switch_disable = left_limit_switch_disable
        self.swap_limit_switches = swap_limit_switches
        self.right_limit_switch_polarity = right_limit_switch_polarity
        self.left_limit_switch_polarity = left_limit_switch_polarity
        self.ref_search_mode = ref_search_mode

    def updateMaxSpeed(self, speed):
        self.max_pos_speed = self.axis_resolution * speed

        # also update acceleration settings
        self.max_acceleration = self.acceleration_factor * self.max_pos_speed
        self.max_deceleration = self.max_acceleration
        self.acceleration_a1 = self.max_acceleration
        self.velocity_v1 = self.max_acceleration
        self.deceleration_d1 = self.max_acceleration

        # also update reference search speeds
        if self.ref_search_speed_factor <= 1 and self.ref_search_speed_factor >= 0:
            self.ref_search_speed = self.max_pos_speed * self.ref_search_speed_factor
        else:
            raise ValueError("Reference search speed factor is unsuitable.")

        if self.ref_switch_speed_factor <= 1 and self.ref_switch_speed_factor >= 0:
            self.ref_switch_speed = self.max_pos_speed * self.ref_switch_speed_factor
        else:
            raise ValueError("Reference switch speed factor is unsuitable.")


class MotorInterface():
    def __init__(self, settings=None):
        if settings is None:
            # choose default settings
            self.settings = Settings()
        else:
            self.settings = settings

        self.status = Status()
        self.mc = MotorCommander()

    def __command__(self, command_name, value=0):
        return self.mc.runMotorCommand(command_name, self.settings.module_address, self.settings.motor_idx, np.int32(value))

    def getTargetPosition(self):
        self.status.targetPosition = self.__command__("gap_target_position")
        return self.status.targetPosition

    def getActualPosition(self):
        self.status.actualPosition = self.__command__("gap_actual_position")
        return self.status.actualPosition

    def __setRunCurrent__(self):
        self.__command__(
            "sap_run_current", (self.settings.run_current/self.settings.module_current)*255)

    def __setStandbyCurrent__(self):
        self.__command__("sap_standby_current",
                         (self.settings.standby_current/self.settings.module_current)*255)

    def __setMicrosteps__(self):
        self.__command__("sap_microsteps", np.log(
            self.settings.microsteps)/np.log(2))

    def __setRefSearchSpeed__(self):
        self.__command__("sap_ref_search_speed",
                         self.settings.ref_search_speed)

    def __setRefSwitchSpeed__(self):
        self.__command__("sap_ref_switch_speed",
                         self.settings.ref_switch_speed)

    def __setMaxPosSpeed__(self):
        self.__command__("sap_max_pos_speed", self.settings.max_pos_speed)

    def __setMaxAcceleration__(self):
        self.__command__("sap_max_acceleration",
                         self.settings.max_acceleration)

    def __setMaxDeceleration__(self):
        self.__command__("sap_max_deceleration",
                         self.settings.max_deceleration)

    def __setAccelerationA1__(self):
        self.__command__("sap_acceleration_a1", self.settings.acceleration_a1)

    def __setVelocityV1__(self):
        self.__command__("sap_velocity_v1", self.settings.velocity_v1)

    def __setDecelerationD1__(self):
        self.__command__("sap_deceleration_d1", self.settings.deceleration_d1)

    def __setVelocityStart__(self):
        self.__command__("sap_velocity_start", self.settings.velocity_start)

    def __setVelocityStop__(self):
        self.__command__("sap_velocity_stop", self.settings.velocity_stop)

    def __setRightLimitSwitchDisable__(self):
        self.__command__("sap_right_limit_switch_disable",
                         self.settings.right_limit_switch_disable)

    def __setLeftLimitSwitchDisable__(self):
        self.__command__("sap_left_limit_switch_disable",
                         self.settings.left_limit_switch_disable)

    def __setSwapLimitSwitches__(self):
        self.__command__("sap_swap_limit_switches",
                         self.settings.swap_limit_switches)

    def __setRightLimitSwitchPolarity__(self):
        self.__command__("sap_right_limit_switch_polarity",
                         self.settings.right_limit_switch_polarity)

    def __setLeftLimitSwitchPolarity__(self):
        self.__command__("sap_left_limit_switch_polarity",
                         self.settings.left_limit_switch_polarity)

    def __setRefSearchMode__(self):
        self.__command__("sap_ref_search_mode", self.settings.ref_search_mode)

    def __enterDownloadMode__(self):
        self.__command__("enter_download_mode")

    def __exitDownloadMode__(self):
        self.__command__("exit_download_mode")

    def __resetApplication__(self):
        self.__command__("reset_application")

    def __runApplication__(self):
        self.__command__("run_application")

    def moveAxis(self, position, relative=True):
        count = self.settings.axis_resolution * position

        if relative:
            self.__command__("mvp_rel", count)

        else:
            self.__command__("mvp_abs", count)

    def waitForPosition(self):
        self.__command__("wait_pos")

    def programmedMovement(self, movements):
        self.__enterDownloadMode__()

        for m in movements():
            self.moveAxis(m.position, m.relative)
            self.waitForPosition()

        self.__exitDownloadMode__()
        self.__resetApplication__()
        self.__runApplication__()

    def updateMovementParameters(self):
        self.__setRefSearchSpeed__()
        self.__setMaxPosSpeed__()
        self.__setMaxAcceleration__()
        self.__setMaxDeceleration__()
        self.__setAccelerationA1__()
        self.__setVelocityV1__()
        self.__setDecelerationD1__()
        self.__setVelocityStart__()
        self.__setVelocityStop__()

    def initialSetup(self):
        self.__setRunCurrent__()
        self.__setStandbyCurrent__()
        self.__setMicrosteps__()
        self.__setRefSearchSpeed__()
        self.__setMaxPosSpeed__()
        self.__setMaxAcceleration__()
        self.__setMaxDeceleration__()
        self.__setAccelerationA1__()
        self.__setVelocityV1__()
        self.__setDecelerationD1__()
        self.__setVelocityStart__()
        self.__setVelocityStop__()
        self.__setRightLimitSwitchDisable__()
        self.__setLeftLimitSwitchDisable__()
        self.__setSwapLimitSwitches__()
        self.__setRightLimitSwitchPolarity__()
        self.__setLeftLimitSwitchPolarity__()
        self.__setRefSearchMode__()
