import sys
import glob
import serial
import numpy as np


class MotorCommand():
    def __init__(self, name, command, subcommand=None):
        self.name = name
        self.command = command
        self.subcommand = subcommand


class MotorCommander():

    def __init__(self):
        self.availablePorts = []
        self.selectedPort = None
        self.serialConnection = serial.Serial()
        self.serialConnection.baudrate = 9600
        self.serialConnection.timeout = 1

        self.motorCommands = [
            MotorCommand("stop", 3, 0),
            MotorCommand("mvp_abs", 4, 0),
            MotorCommand("mvp_rel", 4, 1),
            MotorCommand("rfs", 13, 0),
            MotorCommand("wait_pos", 27, 1),
            MotorCommand("sap_max_pos_speed", 5, 4),
            MotorCommand("sap_max_acceleration", 5, 5),
            MotorCommand("sap_run_current", 5, 6),
            MotorCommand("sap_standby_current", 5, 7),
            MotorCommand("sap_right_limit_switch_disable", 5, 12),
            MotorCommand("sap_left_limit_switch_disable", 5, 13),
            MotorCommand("sap_swap_limit_switches", 5, 14),
            MotorCommand("sap_acceleration_a1", 5, 15),
            MotorCommand("sap_velocity_v1", 5, 16),
            MotorCommand("sap_max_deceleration", 5, 17),
            MotorCommand("sap_deceleration_d1", 5, 18),
            MotorCommand("sap_velocity_start", 5, 19),
            MotorCommand("sap_velocity_stop", 5, 20),
            MotorCommand("sap_right_limit_switch_polarity", 5, 24),
            MotorCommand("sap_left_limit_switch_polarity", 5, 25),
            MotorCommand("sap_microsteps", 5, 140),
            MotorCommand("sap_ref_search_mode", 5, 193),
            MotorCommand("sap_ref_search_speed", 5, 194),
            MotorCommand("sap_ref_switch_speed", 5, 195),
            MotorCommand("gap_target_position", 6, 0),
            MotorCommand("gap_actual_position", 6, 1),
            MotorCommand("run", 129, 0),
            MotorCommand("reset", 131, 0),
            MotorCommand("enter_download_mode", 132, 0),
            MotorCommand("exit_download_mode", 133, 0),
        ]

    def __getCommandName__(self, name):
        for c in self.motorCommands:
            if c.name == name:
                return c

    def __generateMotorCommand__(self, address, motorCommand, motor_idx, value):
        """ 
        Generates a motor command
        """
        # command format:
        # 1 byte module address (0)
        # 1 byte command number
        # 1 byte type number
        # 1 byte motor number
        # 4 bytes value
        # 1 byte checksum (add all other bytes with 8bit addition)

        command = motorCommand.command
        subcommand = motorCommand.subcommand

        cmd = bytes([address, command, subcommand, motor_idx]) + \
            (int(value)).to_bytes(4, byteorder='big', signed=True)

        checksum = int(np.uint8(sum(cmd))).to_bytes(1, byteorder='big')
        cmd += checksum

        return cmd

    def runMotorCommand(self, command_name, address, motor_idx, value):
        if self.serialConnection.is_open:
            motorCommand = self.__getCommandName__(command_name)
            cmd = self.__generateMotorCommand__(
                address, motorCommand, motor_idx, value)

            self.serialConnection.write(cmd)
            response = self.serialConnection.read(9)

            # extract data
            data = int.from_bytes(response[4:7], byteorder='big')

            return data

        else:
            return None

    def closeMotorConnection(self):
        if self.serialConnection.is_open:
            self.serialConnection.close()

    def openMotorConnection(self, port):
        self.closeMotorConnection()
        self.serialConnection.port = port
        self.serialConnection.open()

    def listSerialPorts(self):
        """ Lists serial port names

                :raises EnvironmentError:
                        On unsupported or unknown platforms
                :returns:
                        A list of the serial ports available on the system
        """
        self.closeMotorConnection()

        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        self.availablePorts = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                self.availablePorts.append(port)
            except (OSError, serial.SerialException):
                pass
